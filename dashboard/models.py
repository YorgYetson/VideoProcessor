from django.db import models


class MigratedVideos(models.Model):
    name = models.TextField(blank=True, null=True)
    preprocess_path = models.TextField(blank=True, null=True)
    completed_path = models.TextField(blank=True, null=True, unique=True)
    archive_path = models.TextField(blank=True, null=True, unique=True)
    template_path = models.TextField(blank=True, null=True)
    template_archive_path = models.TextField(blank=True, null=True, unique=True)
    composition_name = models.TextField(blank=True, null=True)
    STATUS_CHOICES = (('DI', 'Discovered'),
                      ('MI', 'Migrated'),
                      ('TC', 'Template Created'),
                      ('PR', 'Processing'),
                      ('CO', 'Completed'),
                      ('DE', 'Deleted'))
    status = models.CharField(max_length=2, choices=STATUS_CHOICES, default='DI')
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)

    def __str__(self):
        return self.name

    class Meta:
        app_label = 'dashboard'
        db_table = 'migrated_videos'
