from django.conf.urls import url
from . import views

urlpatterns = [
    #url(r'^(?P<eventid>\d+)/$', views.EventDetail.as_view()),
    url(r'^$', views.Dashboard.as_view()),
    url(r'^migrate/$', views.StartMigrate.as_view()),
    url(r'^render/(?P<pk>\d+)$', views.StartRender.as_view()),

]
