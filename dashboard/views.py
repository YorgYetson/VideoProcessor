import random
from django.shortcuts import render, redirect
from django.views import View
from django.contrib.auth.decorators import login_required
from django.utils.decorators import method_decorator
from video_manager.taskapp.tasks import (migrate_task, create_video_template_task,
                                         render_template_task, archive_video_task, render_video_task)

from dashboard.models import MigratedVideos


class Dashboard(View):
    def get(self, request, video_id=None):
        videos = MigratedVideos.objects.all()
        return render(request, "pages/home.html", {"videos": videos})


class StartMigrate(View):
    def get(self, request):
        unprocessed = migrate_task.delay().get()
        for video in unprocessed:
            v = MigratedVideos(preprocess_path=video)
            v.save()
        return redirect("/dashboard/")


class MigrateAndProcess(View):
    def get(self, request):
        unprocessed = migrate_task.delay().get()
        for video in unprocessed:
            template = create_video_template_task.delay(video).get()
            output_dir = '/Users/yorg/Processed Videos/'
            output = output_dir + video.split("/")[-1].split(".mp4")[0] + ".avi"
            composition = 'Render Transition ' + str(random.randint(1, 12)).zfill(2)
            render_template_task.delay(template, composition, output)
        return redirect("/dashboard/")
        return render(request, "pages/home.html")


class StartRender(View):
    def get(self, request, pk=None):
        render_video_task.delay(pk)
        return redirect("/dashboard/")
