import os
from shutil import copyfile
from ffprobe3 import FFProbe
import fileinput
import subprocess
import random


def migrate(source, destination):
    """ Copies files from the source folder to the destination folder then deletes the source files """

    # Get a list of the files in the source dir
    file_paths = []
    for path, subdirs, files in os.walk(source):
        for name in files:
            file_paths.append([path, name])

    # Loop through the files and copy them to the destination
    for path in file_paths:
        src = path[0] + "/" + path[1]
        dir = destination + path[0].split("/")[-1]
        dst = destination + path[0].split("/")[-1] + "/" + path[1]

        # Check if the file was already copied successfully but not deleted
        if os.path.exists(dst):

            # Check if the copied file is the same size
            if os.stat(src).st_size == os.stat(dst).st_size:
                os.remove(src)
                continue

        # Make sure the directory exists
        if not (os.path.isdir(dir)):
            os.mkdir(dir)

        # Copy the file
        copyfile(src, dst)

        # Check to see if copy was successful and delete file
        if os.path.exists(dst):

            # Check if the copied file is the same size
            if os.stat(src).st_size == os.stat(dst).st_size:
                os.remove(src)

    file_paths = []
    for path, subdirs, files in os.walk(destination):
        for name in files:
            if name != '.DS_Store':
                file_paths.append(path + "/" + name)
    return file_paths


def duration(filename):
    """returns the length of a video in seconds """

    return FFProbe(filename).streams[0].duration


def replace(file_path, search_text, replace_text):
    """ Searches a file for the search_text and replaces it with the replace_text
        Used to replace the video placeholder path in an XML After Effects template """

    with fileinput.FileInput(file_path, inplace=True, backup='.bak') as file:
        for line in file:
            print(line.replace(search_text, replace_text), end='')


def render(template_path, composition, output_path):
    """ Creates a subprocess to render the video """

    aerender = '/Applications/Adobe After Effects CC 2018/aerender'
    subprocess.call([aerender, '-project', template_path, '-comp', composition, "-OMtemplate", "autopro", '-output', output_path])




if __name__ == "__main__":
    # Migration Source Folder
    source = "/Volumes/record/"

    # Migration Destination Folder
    destination = "/Users/yorg/record/"

    # Migrate files and get a list of unprocessed files
    unprocessed = migrate(source, destination)

    # Define the placeholder path to be replaced
    placeholder = "/Users/yorg/record/Fortnite/Fortnite.mp4"

    # Define the Output directory
    output_dir = '/Users/yorg/Processed Videos/'

    for video in unprocessed:
        # Set the template
        template = '/Users/yorg/Desktop/test.aepx'

        # Choose one of the 12 comps available in the template.
        composition = 'Render Transition ' + str(random.randint(1, 12)).zfill(2)

        # Make a new template so we can reuse the original on simultaneous processes
        temp_template = '/Users/yorg/video temp/' + video.split("/")[-1] + template.split("/")[-1]
        copyfile(template, temp_template)

        # Replace the placeholder path in the AE file with the video path
        replace(temp_template, placeholder, video)

        # Define the output path based on the original video name
        output = output_dir + video.split("/")[-1].split(".mp4")[0] + ".avi"

        # Process the video
        render(temp_template, composition, output)
