from __future__ import absolute_import
import os

from celery import Celery
from django.apps import apps, AppConfig
from django.conf import settings
from video_manager.utils import migrate, duration, replace, render
from shutil import copyfile
import random
from dashboard.models import MigratedVideos
from video_manager.taskapp.celery import app

@app.task
def migrate_task(source="/Volumes/record/", destination="/Users/yorg/record/"):
    try:
        unprocessed = migrate(source, destination)
        return unprocessed
    except Exception as e:
        print(e)


@app.task
def create_video_template_task(video, composition=None, template='/Users/yorg/Desktop/test.aepx', placeholder='/Users/yorg/record/Fortnite/Fortnite.mp4'):
    try:
        temp_template = '/Users/yorg/video temp/' + video.split("/")[-1] + template.split("/")[-1]
        copyfile(template, temp_template)
        replace(temp_template, placeholder, video)
        if composition == None:
            composition = 'Render Transition ' + str(random.randint(1, 12)).zfill(2)
        return temp_template, composition
    except Exception as e:
        print(e)


@app.task
def archive_video_task(video_path, template_path, archive_folder):
    video_archive_path = archive_folder + video_path.split("/")[-1]
    copyfile(video_path, video_archive_path)
    os.remove(video_path)

    template_archive_path = archive_folder + "/templates/" + template_path.split("/")[-1]
    copyfile(template_path, template_archive_path)
    os.remove(template_path)
    return video_archive_path, template_archive_path


@app.task
def render_template_task(template_path, composition, output_path, video, archive_folder='/Users/yorg/video archive/'):
    render(template_path, composition, output_path)
    video_archive_path, template_archive_path = archive_video_task.delay(video, template_path, archive_folder).get()
    return video_archive_path, template_archive_path

@app.task
def render_video_task(pk):
    video = MigratedVideos.objects.get(pk=pk)

    if video.composition_name:
        comp = video.composition_name
    else:
        comp = None

    template, composition = create_video_template_task.delay(video.preprocess_path, comp).get()

    archive_folder = '/Users/yorg/video archive/'
    output_dir = '/Users/yorg/Processed Videos/'
    output_path = output_dir + video.preprocess_path.split("/")[-1].replace(".mp4", ".mov")
    render(template, composition, output_path)
    video.completed_path = output_path
    video.save()

    video_archive_path, template_archive_path = archive_video_task.delay(video.preprocess_path, template, archive_folder).get()
    video.template_archive_path = template_archive_path
    archive_path = video_archive_path
    video.save()


@app.task(bind=True)
def debug_task(self):
    print('Request: {0!r}'.format(self.request))  # pragma: no cover
