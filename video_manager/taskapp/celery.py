
from __future__ import absolute_import
import os

from celery import Celery
from django.apps import apps, AppConfig
from django.conf import settings
from video_manager.utils import migrate, duration, replace, render
from shutil import copyfile
import random




if not settings.configured:
    # set the default Django settings module for the 'celery' program.
    os.environ.setdefault('DJANGO_SETTINGS_MODULE', 'config.settings.local')  # pragma: no cover


app = Celery('video_manager')


class CeleryConfig(AppConfig):
    name = 'video_manager.taskapp'
    verbose_name = 'Celery Config'

    def ready(self):
        # Using a string here means the worker will not have to
        # pickle the object when using Windows.
        app.config_from_object('django.conf:settings')
        installed_apps = [app_config.name for app_config in apps.get_app_configs()]
        app.autodiscover_tasks(lambda: installed_apps, force=True)
        #from dashboard.models import MigratedVideos
        #MigratedVideos = apps.get_model('dashboard', 'MigratedVideos')
        #MigratedVideos = self.get_model(dashboard.models.MigratedVideos)
